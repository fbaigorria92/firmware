/*
 * ADS1115.h
 *
 *  Created on: 11 de jun. de 2020
 *      Author: nicob
 *      Refactor: Federico Baigorria
 */

#ifndef INC_ADS1115_H_
#define INC_ADS1115_H_

#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include <string.h>

extern xQueueHandle adsQueue;

///! I2C peripheral settings
#define I2C_DEVICE		I2C1
#define I2C_DEVICE_NUM	1
#define I2C_DEVICE_PORT	0
#define I2C_SDA_PIN		0
#define I2C_SCL_PIN		1
#define I2C_SPEED		400000

///! ADS1115 commands
//#define WRITE 		0x00
//#define READ 			0x01
//#define CONFIG			0x42E3	///! No se utiliza
#define CONFIG_DISPARO 	0xC283	///! Se utiliza
#define LO_THRESH 		0x0000
#define HI_THRESH 		0xFFFF
#define APR_0 			0x00
#define APR_1 			0x01
#define APR_2 			0x02
#define APR_3 			0x03
#define APR_R 			0x06
#define ADDR			0x48

#define BUFSIZE         64
#define RD_BIT          0x01

///! ADS1115 references
#define ADS1115_GAIN 		1.0f
#define ADS1115_VREF 		3.3f
#define ADS1115_MAX_COUNTS 	32768

///! ADS1115 Mux settings for reading single ended channel
typedef enum {
	adsAin0 = 0x04,
	adsAin1 = 0x05,
	adsAin2 = 0x06,
	adsAin3 = 0x07
} adsMux;

///! Public functions
void adsInit(void);
void adsConfig(void);
uint16_t adsGetCounts(adsMux);
float adsGetVoltage(uint16_t);
///! Task
void vTaskADC(void*);

#endif /* INC_ADS1115_H_ */
