/*
 * Pulsadores.h
 *
 *  Created on: 9 de jun. de 2020
 *      Author: nicob
 */

#ifndef INC_PULSADORES_H_
#define INC_PULSADORES_H_


#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"

#define SW1_PORT 		0
#define SW1_PIN			21

#define SW2_PORT		0
#define SW2_PIN			3

#define SWNULL			0
#define	SW1				1
#define SW2				2

#define PRESS			0
#define DEBOUNCE_TIME	70

void Pulsadores_Init(void);
void EINT_IRQHandler(void);
uint8_t Leer_Pulsadores(void);
void vTaskDebounce (void *pV);

#endif /* INC_PULSADORES_H_ */
