#ifndef _SD_SPI_H_
#define _SD_SPI_H_

///! SD Files definition
#define SD_FILENAME "LOG.TXT"
#define SD_FILECLK 	"CLK.TXT"

///! Set to 1 if PROCTIMER is used for querying the SD card status (by external SD card pin)
#define SD_PROCTIMER	0
///! Hardware SPI
#define	_MY_SSP		LPC_SSP1	///! SSP utilizado. Todo alterar mmc.c para independizar perifericos
#define	_SD_SCK		0,7			///! SCK
#define _SD_MISO	0,8			///! MISO
#define _SD_MOSI 	0,9			///! MOSI
#define	_SD_CS		0,6			///! CS Manual. Modificarlo en mmc.c dentro de fatfs_ssp

/** DO NOT EDIT
 */

///! Mandatory includes (FatFS and board)
#include "board.h"
#include "../../../modules/lpc1769/fatfs_ssp/inc/ff.h"
///! Self includes
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

///! Defines
#define SD_VFULL 1
#define SD_VLEFT 0
#define SD_ISMOUNT		0x01
#define SD_FILE_EXISTS	0x02
#define SD_ISLOGGING	0x04

///! Application dependent
///! Queue for accessing the sdCardStatus variable
extern xQueueHandle sdRealStatus;
///! SD Logging task handler
extern xTaskHandle pxSDLog;
///! Queue for logging data into the SD
extern xQueueHandle sdQueueLogData;

///! Special public function (do not use in user defined application)
//void disk_timerproc(void);
///! Public functions
void appSDHardwareInit(void);
void appSetTasks(void);
uint8_t appGetSDStatus(void);
///! Task functions
void vTaskSDMonitor(void *);
void vTaskSDMLogging(void *);

#ifdef SD_PROCTIMER
void vTaskProcTimer(void *pvT);
#endif

#endif /* #ifndef _SD_SPI_H_ */
