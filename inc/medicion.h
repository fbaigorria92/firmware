 /**
	\file medicion.h
	\brief Librerias para medicion.c
	\author UTN_ME1_R4051_Grupo1
	\date 2018.10.19
*/


//<! DEFINES 
//----------------------- CONSTANTES DE ECUACION DE CURVA CARACTERISTICA DEL SENSOR
//#define	A	312
//#define	B	(-595)
//#define	C	514
//#define	D	(-206)
//#define	F	(30.7)

//#define	OFFSET_CM	0		// EN CASO DE SER NECESARIO APLICAR UN OFFSET EN ALGUNA ECUACION
//#define	OFFSET_CUENTAS	0

#define CUENTA_ADC_VOLTS	(1)		//VALOR DE 1 CUENTA ADC EQUIVALENTE EN VOLTS
#define SIZE			64 //32 double //256 uint8_t//64 para float

//<! PROTOTIPOS DE FUNCIONES
float countsToEC(float);


