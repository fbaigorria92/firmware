#ifndef _MAIN_H_
#define _MAIN_H_

///! Application includes

///! Others
#include "math.h"

///! OS: FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

///! Hardware
#include "board.h"
#include "switches.h"
#include "PWM.h"

///! Modules
#include "ff.h"
#include "../../../modules/lpc1769/ssd1306/inc/ssdOLED.h"
#include "../../../modules/lpc1769/screenCycle/inc/scCycle.h"
#include "../inc/sdLogging.h"
#include "../../../modules/lpc1769/tstamp/inc/tsRTC.h"
#include "ADS1115.h"
#include "medicion.h"

///! SD card/file flag status
xQueueHandle sdRealStatus = NULL;

///! Firmware version
#define VERFIRM	"0.04"

///! SSDOLED symbols offset
#define APP_SDCARD_OFFS 	99
#define APP_FILE_OFFS 		106

#endif /* #ifndef _MAIN_H_ */
