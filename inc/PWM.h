/*
 * PWM.h
 *
 *  Created on: 16 ago. 2020
 *      Author: nicob
 */

#ifndef INC_PWM_H_
#define INC_PWM_H_

#include "chip.h"
/**
 * @brief PWM1 register block structure
 */
typedef struct{
	__IO uint32_t IR;		/*!< Interrupt Register */
	__IO uint32_t TCR; 		/*!< Timer Control Register */
	__IO uint32_t TC;		/*!< Timer Counter	*/
	__IO uint32_t PR;		/*!< Prescale Register */
	__IO uint32_t PC;		/*!< Prescale Counter */
	__IO uint32_t MCR;		/*!< Match Control REgister */
	__IO uint32_t MR0;		/*!< Match Register 0 */
	__IO uint32_t MR1;		/*!< Match Register 1 */
	__IO uint32_t MR2;		/*!< Match Register 2 */
	__IO uint32_t MR3;		/*!< Match Register 3 */
	__IO uint32_t CCR;		/*!< Capture Control Register */
	__I uint32_t CR0;		/*!< Capture Register 0 */
	__I uint32_t CR1;		/*!< Capture Register 1 */
	__I uint32_t CR2;		/*!< Capture Register 2 */
	__I uint32_t CR3;		/*!< Capture Register 3 */
		uint32_t RESERVED0;
	__IO uint32_t MR4;		/*!< Match Register 4 */
	__IO uint32_t MR5;		/*!< Match Register 5 */
	__IO uint32_t MR6;		/*!< Match Register 6 */
	__IO uint32_t PCR;		/*!< PWM Control Register */
	__IO uint32_t LER;		/*!< Load enable Register */
    	 uint32_t RESERVED1[7];
	__IO uint32_t CTCR;		/*!< Count Control Register */
}LPC_PWM1_T;

/**
 * @brief Pins of pwm's channels, to port1 and port2
 */

#define PUERTO2	1

#if defined(PUERTO1)

		#define PWM1_CH1OUT				1,18
		#define PWM1_CH2OUT				1,20
		#define PWM1_CH3OUT				1,21
		#define PWM1_CH4OUT				1,23
		#define PWM1_CH5OUT				1,24
		#define PWM1_CH6OUT				1,26

		#define FUNC_PWM             	2

#elif defined(PUERTO2)
		#define PWM1_CH1OUT				2,0
		#define PWM1_CH2OUT				2,1
		#define PWM1_CH3OUT				2,2
		#define PWM1_CH4OUT				2,3
		#define PWM1_CH5OUT				2,4
		#define PWM1_CH6OUT				2,5

		#define FUNC_PWM             	1

#endif

#define	CH1								1
#define	CH2								2
#define	CH3								3
#define	CH4								4
#define	CH5								5
#define	CH6								6

#define LPC_PWM1_BASE             		0x40018000

#define LPC_PWM1				  		((LPC_PWM1_T *) LPC_PWM1_BASE)

#define PWMPRESCALE						23	// 1uS resolution
#define FREC1KHZ						1000
#define FREC2KHZ						500

void vPWMHardwareInit(uint32_t);
void vPWMInit(void);
void vPWMUpdate(uint32_t);


#endif /* INC_PWM_H_ */
