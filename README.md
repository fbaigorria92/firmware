# Medidor de TDS: Projecto PBC
Repositorio del desarrollo de hardware  

## Â¿Como arrancar?
Para Windows existe una herramienta que permite ejecutar comandos de git en linux mediante su propio shell. Puede descargarse accediendo al siguiente hipervinculo [Git for Windows](https://gitforwindows.org/).

Si se trabaja en alguna distro de Linux se debe solamente instalar el paquete de git en caso de no estar previamente instalado.

Finalmente en la consola de git nos ubicamos en el directorio de trabajo donde clonar este repositorio y ejecutamos lo siguiente

git clone https://fbaigorria92@bitbucket.org/fbaigorria92/hardware.git

---

## Â¿Como trabajar en este repositorio?

Trabajaremos sobre una rama (branch a partir de ahora) master la cual tendra **siempre** la ultima informaciÃ³n vÃ¡lida y funcional del proyecto. En base al branch master individualmente cada miembro del equipo creara su propio branch para aÃ±adir o modificar archivos esquemÃ¡ticos, de PCB, footprints, etc.

Al momento de finalizar con el trabajo en su correspondiente branch se harÃ¡ un **pull request** hacia la rama master. Esto no es inmediato y el Ãºnico que podrÃ¡ aprobar los pull requests serÃ¡ el propietario del proyecto (es decir Federico).

En cada branch particular todos son libres de hacer lo que quiera, nadie podrÃ¡ en cambio pushear directamente al master.

---

## Nomenclaturas

Para mantener el orden y facilitar las busquedas en bitbucket mantendremos una serie de nomenclaturas a la hora de crear branches. Para simplificar las tareas de creaciÃ³n de branches no utilizaremos issues. No obstante su uso es positivo para mantener el orden y crear discusiones antes de crear un branch por un motivo determinado.

Mantendremos tres tipos de branches, cada uno con su caracterÃ­stica propia. La sintaxis de cada branch serÃ¡ <branch_type>/<branch_name>. Por ejemplo bugfix/Corrijo_diseÃ±o_circuito_fuente_de_alimentacion

* **bugfix** : Como su nombre lo indica, se crea este tipo de branch para atender correcciones de bugs detectados en el diseÃ±o.
* **feature** : Desarrollo de una nueva funcionalidad, o en nuestro caso de un nuevo bloque o circuito.

---

## Uso basico de git

Luego de clonar el repositorio ya estamos en condiciones de comenzar a trabajar. A continuaciÃ³n dejo algunos escenarios sobre como usar git.

** Crear un nuevo branch de trabajo  **

Seguramente lo primero que haremos luego de clonar nuestro repositorio es crear un branch particular para trabajar. Una  forma de hacer esto es.

git checkout -b "nombre de mi branch"

Ejemplo: git checkout -b "bugfix/Corrijo_diseÃ±o_circuito_fuente_de_alimentacion"

** Preparar archivos para subir al repositorio **

Finalizados los cambios deseados en nuestro branch debemos preparar los archivos que queremos que se suban a nuestro repositorio remoto, esto suele ser el primer paso antes de realizar un push al repo. Para esto hacemos:

git add <path/file.c> **(sin <>)**

Ejemplo: git add README.md

Se desaconseja el uso de git add . dado que no hay control sobre lo que se sube. Subir archivos innecesarios generan problemas de merge a futuro.

** Preparar un commit **

Antes de preparar un commit el paso necesario es indicar que archivos seran pusheados al repo. Sin cambios no hay commit para hacer. Luego de agregar los cambios (git add)  tenemos que preparar un commit.  Una forma simple es:

git commit -m  "Explicacion breve sobre que consiste el commit"

Ejemplo: git commit -m "El regulador elegido era incorrecto. Se cambia por el  LM7805"

Por simplicidad no se exige un formato estÃ¡ndar de los commits, pero si se utiliza el one line commit como es commit -m por favor hacer un commit muy conciso y claro. Para hacer commits claros, concretos, prolijos e informativos aca hay un link muy interesante sobre este tema [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)

Si el commit fuera excesivamente detallado, por favor hacer un BUEN commit como se explica en el link anterior.

** Enviar al repo  **

Finalmente una vez preparado nuestro commit (git add y git commit) tenemos que hacer un push al repo. Lo mas simple de hacer es:

git push

En caso de suceder algun error (seguramente en el set upstream) leer el mensaje de consola y ejecutar el push tal cual lo indica. Tambien se puede hacer:

git push origin <branch>

Ejemplo: git push origin bugfix/Corrijo_diseÃ±o_circuito_fuente_de_alimentacion

** Cambiar de branch  **

Cuando quiero cambiar de branch  lo  que puedo hacer es ejecutar la siguiente instruccion. Tener en cuenta que esto NO crea un branch como git checkout -b sino que hace un checkout al branch declarado. Hacer un checkout implica cambiar de branch pero no actualiza nuestro git local con todos los cambios remotos.

git checkout <new_branch>

Por tanto es recomendable, en caso de tener dudas sobre los ultimos cambios (referencias) en el repo remoto hacer un:

git fetch && git checkout <new_branch>

Ejemplo: git fetch && git checkout master

Donde git fetch nos trae todos los cambios (referencias) de nuestro repositorio remoto. Aclaro referencias, hacer un fetch NO altera los cambios locales de nuestros archivos sino que solamente actualiza nuestro git con los ultimos branches, tags, etc aÃ±adidos al repositorio incluyendo sus cambios remotos de los archivos.

Finalmente cuando cambiamos de branch tenemos que realizar un pull para efectivamente actualizar nuestro local con los cambios del repositorio remoto. Esto si altera nuestros archivos locales y por tanto pueden ocurrir ** conflictos de merge **

git pull.

Resumen:

1. git fetch && git checkout <new_branch>
2. git pull

** Ver el estado de nuestro local **

Para saber que archivos estan en stage, cuales estan untracked y cuales son los ultimos archivos modificados sin aÃ±adir al stage podemos ejecutar la siguiente instruccion. Generalmente esto es un paso anterior a git add para saber que aÃ±adir.

git status

Para NO visualizar todos los archivos untracked dado que son aquellos que no nos interesa subir al repositorio pero somos demasiado vagos para agregarlos en la lista de excepciones podemos hacer:

git status -uno

-uno: untracked  files no

** Guardar cambios locales momentaneamente sin pushear al repo **

En caso de estar trabajando en un branch con cambios locales que NO queremos pushear al repositorio por motivos personales o bien porque necesitamos cambiar a otro branch y nuestros cambios locales son muy "sucios" para subirlos podemos realizar lo siguiente:

git stash save "nombre de mi nuevo stash"

Esto nos permite guardar todos los cambios locales en un "stash" para que, al regresar al branch desde el que generamos el stash, podamos recuperar estos cambios. Recuperamos los cambios haciendo un:

git stash por stash@{N}

Donde N es el numero de stash que queremos recuperar. Para conocer todos los stashes que tenemos podemos hacer:

git stash list

Esta forma de trabajar si bien no es mala, hay que mantener un orden y ser cuidadoso a la hora de stashear. Sobre todo, es recomendable no hacer pop en branches distintos para evitar conflictos de merge.

