/*
 * PWM.c
 *
 *  Created on: 16 ago. 2020
 *      Author: nicob
 */

#include "PWM.h"
extern uint32_t Tperiod;
/*
 * Inicializa el canal channel con función PWM
 *
 * Arguments:
 * 	channel: canal a inicializar para PWM
 *
 * Return:
 * 	void
 *
 */
void vPWMHardwareInit(uint32_t channel)
{

	switch(channel)
	{
	case CH1:
		Chip_IOCON_PinMux(LPC_IOCON,PWM1_CH1OUT, IOCON_MODE_INACT, FUNC_PWM);
		break;

	case CH2:
		Chip_IOCON_PinMux(LPC_IOCON,PWM1_CH2OUT, IOCON_MODE_INACT, FUNC_PWM);
		break;

	case CH3:
		Chip_IOCON_PinMux(LPC_IOCON,PWM1_CH3OUT, IOCON_MODE_INACT, FUNC_PWM);
		break;

	case CH4:
		Chip_IOCON_PinMux(LPC_IOCON,PWM1_CH4OUT, IOCON_MODE_INACT, FUNC_PWM);
		break;

	case CH5:
		Chip_IOCON_PinMux(LPC_IOCON,PWM1_CH5OUT, IOCON_MODE_INACT, FUNC_PWM);
		break;

	case CH6:
		Chip_IOCON_PinMux(LPC_IOCON,PWM1_CH6OUT, IOCON_MODE_INACT, FUNC_PWM);
		break;

	default:
		break;
	}

}

/*
 * Habilita la salida PWM1 OUT2 como PWM, por defecto se carga cualquier valor, se puede modificar
 * con vPWMUpdate. La resolución y el PR esta calculado para 1 uS (TC aumenta cada 1 uS)
 *
 * No modificar PCR y PR
 * MR0 es el periodo en uS
 * MRx es el usado dependiendo de la salida que se use. OUT1->MR1 etc
 * LER = (1<< Canal)||(1<<0)
 * PCR habilita la salida usada
 * Arguments:
 * void
 *
 * Return:
 * void
 */
void vPWMInit(void)
{

	LPC_PWM1->PCR = 0;
	LPC_PWM1->PR = PWMPRESCALE;
	LPC_PWM1->MR0 = 2000;
	//LPC_PWM1->MR2 = 1000;
	LPC_PWM1->MR1 = 1000;

	LPC_PWM1->MCR = (1 << 1);
	LPC_PWM1->LER = (1 << 1) || (1 << 0);
	LPC_PWM1->PCR = (1 << 9);//9 chn1 10 ch2
	LPC_PWM1->TCR = (1 << 1);
	LPC_PWM1->TCR = (1 << 0) || (1 << 3);

}

/*
 * Modifica el periodo de la salida PWM, siempre DC = 50%.
 *
 * Arguments:
 * vPeriod: período de la señal en uS
 * 			1kHz = 1000
 *
 * MR0 es el periodo
 * MRx es la mitad para hacerlo al 50%
 *
 * Return:
 * void
 */
void vPWMUpdate(uint32_t vPeriod)
{
	LPC_PWM1->MR0 = vPeriod;
	LPC_PWM1->MR1 = vPeriod/2;

	LPC_PWM1->MCR = (1 << 1);
	LPC_PWM1->LER = (1 << 1) || (1 << 0);
	LPC_PWM1->PCR = (1 << 9);//9 chn1 10 ch2
	LPC_PWM1->TCR = (1 << 1);
	LPC_PWM1->TCR = (1 << 0) || (1 << 3);


}
