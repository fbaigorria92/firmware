#include "main.h"

static void initHardware(void)
{
	///! Board init
    Board_Init();
    SystemCoreClockUpdate();
    SysTick_Config(SystemCoreClock / 1000);
	///! Switch init
	Pulsadores_Init();
    ///! SD Card init
    Board_SSP_Init(LPC_SSP1);
    Chip_SSP_Init(LPC_SSP1);
    Chip_SSP_Enable(LPC_SSP1);
    ///! SSD1306 OLED init PO.27 SDA0 cable Gris P0.28 SCL0	cable violeta
	Board_I2C_Init(I2C0);
	Chip_I2C_SetClockRate(I2C0, 1000000);
	Chip_I2C_SetMasterEventHandler(I2C0, Chip_I2C_EventHandlerPolling);
	///! PWM1 CH2 init
	vPWMHardwareInit(CH1);
	vPWMInit();
    ///! ADC init: SCL cable blanco y SDA cable negro
	adsInit();
	///! Display init presentation
	ssdOledInit();
	ssdOledCleanScreen();
	ssdOnResetMsg(VERFIRM);
}

static void vTaskMain(void *pvMT){
	uint8_t sdLocalCard;

	///! Startup message delay
	vTaskDelay(2000/portTICK_RATE_MS);
	ssdOledCleanScreen();

	///! Init by default log interval (needed for avoid task starvation)
	///! TODO: Update by reading the Flash memory and select if must be inited by default or by flash
	scInitDefaultAppStack();

	while(1){
		///! Update SD status
		if(uxQueueMessagesWaiting(sdRealStatus) > 0){
			xQueueReceive(sdRealStatus, &sdLocalCard ,0);
			///! SD Card mount status
			if(sdLocalCard & SD_ISMOUNT){
				ssdOledSetPageOffset(S1306_PAGE0, APP_SDCARD_OFFS);
				ssdOledSetSymbol(ssdSDCardSymb);
			} else {
				ssdOledSetPageOffset(S1306_PAGE0, APP_SDCARD_OFFS);
				ssdOledSetSymbol(ssdNoSymb);
			}
			///! Log file status
			if(sdLocalCard == (SD_ISLOGGING | SD_FILE_EXISTS | SD_ISMOUNT)){
				ssdOledSetPageOffset(S1306_PAGE0, APP_FILE_OFFS);
				ssdOledSetSymbol(ssdFileSymb);
			}else{
				ssdOledSetPageOffset(S1306_PAGE0, APP_FILE_OFFS);
				ssdOledSetSymbol(ssdNoSymb);
			}
		}
		///! Cycle screen
		scScreenTask();
	}

	vTaskDelete(NULL);
}

int main(void){
	///! Hardware init
	initHardware();
	///! Set ADC
	adsConfig();
	///! Set SD
	appSetTasks();

	///! Task creation
	xTaskCreate(vTaskMain, (const char *)"vTaskMain", configMINIMAL_STACK_SIZE*2, NULL, tskIDLE_PRIORITY+1, NULL);

	///! Other tasks not listed here
	// vTaskADC: In ADS1115.c tskIDLE_PRIORITY+2
	// vTaskDebounce: In switches.c tskIDLE_PRIORITY+3
	// vTaskSDMonitor: In sdLogging.c tskIDLE_PRIORITY+2
	// vTaskSDMLogging: In sdLogging.c tskIDLE_PRIORITY+2

	// Scheduler
	vTaskStartScheduler();

    while(1);
}
