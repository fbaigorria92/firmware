/*
 * ADS1115.c
 *
 *  Created on: 11 de jun. de 2020
 *      Author: nicob
 *      Refactor: Federico Baigorria
 */

#include "ADS1115.h"
#include "medicion.h"

///! Private prototypes
static void adsSetRegister(adsMux);

static float pMens;

///! ADS1115 queue
xQueueHandle adsQueue;

static void adsSetRegister(adsMux adsChannel){
	uint16_t adsSetConfig = (CONFIG_DISPARO & (~ 0x7000)) | (adsChannel << 12);
	uint8_t adsBuf[] = {
			APR_1,								///! APR Config register operation
			(adsSetConfig >> 0x08) & 0x00FF,	///! Config setting High byte
			adsSetConfig & 0x00FF				///! Config setting Low byte
	};
	///! I2C transmission
	Chip_I2C_MasterSend(I2C1, ADDR, adsBuf, sizeof(adsBuf)/sizeof(uint8_t));
}

void adsInit(void){
	Chip_IOCON_PinMux(LPC_IOCON, I2C_DEVICE_PORT, I2C_SDA_PIN, IOCON_MODE_INACT, IOCON_FUNC3);
	Chip_IOCON_PinMux(LPC_IOCON, I2C_DEVICE_PORT, I2C_SCL_PIN, IOCON_MODE_INACT, IOCON_FUNC3);
	Chip_IOCON_EnableOD(LPC_IOCON,I2C_DEVICE_PORT, I2C_SDA_PIN);
	Chip_IOCON_EnableOD(LPC_IOCON,I2C_DEVICE_PORT, I2C_SDA_PIN);

	Chip_I2C_Init(I2C_DEVICE_NUM);
	Chip_I2C_SetClockRate(I2C_DEVICE_NUM,I2C_SPEED);
	Chip_I2C_SetMasterEventHandler(I2C1,Chip_I2C_EventHandlerPolling);
}

/**
@fn void adsConfig(void)
@detail ADS1115 setting configuration. CONFIG_DISPARO macro sets the operating mode.
		Starts single conversion 	1	: b15
		AINP = AIN0 and AINN = GND 	100	: b14-b12
		FSR = ±4.096 V				001	: b11-b09
		Continuous conversion mode	  0	: b08
		Data rate setting 128 SPS   100 : b07-b05
		Traditional comparator mode   0 : b04
		Comparator polarity low	      0 : b03
		Non-latching comparator		  0 : b02
		Disable comparator and
		set ALERT/RDY pin to
		high-impedance 				 11 : b01-b00

@return void
*/
void adsConfig(void){
	///! By default start converting channel 0
	adsSetRegister(adsAin0);

	adsQueue = xQueueCreate(1,sizeof(float));
	xTaskCreate(vTaskADC, (signed const char *) "vTaskADC", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, 0);
}

uint16_t adsGetCounts(adsMux adsChannel){
	uint8_t adsBuf[2];
	///! Set new reading channel
	adsSetRegister(adsChannel);
	///! APR Conversion register operation
	Chip_I2C_MasterCmdRead(I2C1, ADDR, APR_0, adsBuf, sizeof(adsBuf)/sizeof(uint8_t));

	return (adsBuf[0] << 8) | adsBuf[1];
}

inline float adsGetVoltage(uint16_t adsChnCounts){
	return ((float) (adsChnCounts / ADS1115_MAX_COUNTS)) * ADS1115_VREF;
}

void vTaskADC(void *pV){
	uint16_t adsCounts[2];

	while(1){
		adsCounts[0] = adsGetCounts(adsAin3);
		vTaskDelay(100/portTICK_RATE_MS);
		adsCounts[1] = adsGetCounts(adsAin0);
		pMens = ( (float)(adsCounts[1] - adsCounts[0]) ) / ( (float)(adsCounts[0]) );
		pMens *= 2200;

		xQueueSend(adsQueue, &pMens, portMAX_DELAY);
		vTaskDelay(1000/portTICK_RATE_MS);
	}

	vTaskDelete(NULL);
}
