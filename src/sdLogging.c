/*!
 *  @file 	   sdLogging.c
 *  @brief     Hardware dependent configuration for SSD1306 OLED display
 *  @details   Firmware functions for SSD1306 hardware init, those include:
 *  		   Necesary hardware settiings for I2C communication.
 *  		   I2C data transmission.
 *
 *  @author    Federico Baigorria
 *  @version   0.2.0
 *  @date      03-24-2020
 *  @copyright GNU Public License.
 */

#include "../inc/sdLogging.h"
#include "../../../modules/lpc1769/screenCycle/inc/scTypes.h"
#include "../../../modules/lpc1769/tstamp/inc/tsRTC.h"

///! SD Card management
static int appSDMount(FATFS *);
static void appSDUnmount(FATFS *appLogv);
static int appSDOpen(FIL *, const char *);
static void appSDClose(FIL *appFile);
///! SD Card status
static void appSDCardState(FATFS *, FIL *, const char *);
static void appSDUpdateLogStatus(void);
///! SD Card operations
static uint8_t appSDWrite(FIL *, const uint8_t *, UINT);
///! SD Card logging management
static uint8_t sdFormatLogBuffer(uint8_t *, scDatalogCfg);
static void sdStrCpy(uint8_t *, uint8_t *, uint8_t *);
static void sdFormatLogSeparator(uint8_t *, uint8_t *);

///! Semaphore handle for SD card resource
xSemaphoreHandle sdMonitorSem;

/* Status flag (set)
 * b0: SD card has been mounted
 * b1: Logging file has been successfully opened/created
 * b2: Datalogging enabled
 */
static uint8_t sdCardStatus;
static FATFS ssdVolume;
static FIL ssdFile;

/** Public functions
 */

#ifdef  SD_PROCTIMER
/**
@fn void vTaskProcTimer(void *pvT)
@detail Timer task for disk_timerproc(). It should be called each 10ms. Useful when used along WP and CardPresent
		signals. This task should have the maximum priority in an OS environment.

@return
*/
void vTaskProcTimer(void *pvT)
{
	portTickType xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();

	while(1){
		disk_timerproc();
		vTaskDelayUntil(&xLastWakeTime, 10/portTICK_RATE_MS);
	}
}

#endif

void appSetTasks(void){
	xTaskCreate(vTaskSDMonitor, (const char *)"vTaskSDMonitor", configMINIMAL_STACK_SIZE*4, NULL, tskIDLE_PRIORITY+2, NULL);
	xTaskCreate(vTaskSDMLogging, (const char *)"vTaskSDMLogging", configMINIMAL_STACK_SIZE*4, NULL, tskIDLE_PRIORITY+2, &pxSDLog);

	vTaskSuspend(pxSDLog);
	///! Semaphore arbitration between SD monitoring and datalogging
	vSemaphoreCreateBinary(sdMonitorSem);
	///! Queue for accessing the sdCardStatus flag variable (requested by vTaskMain)
	sdRealStatus = xQueueCreate(1, sizeof(uint8_t));
	///! Queue for requesting the appStack data (required to vTaskMain)
	sdQueueLogData = xQueueCreate(1, sizeof(scDatalogCfg));
}

/**
@fn void vTaskProcTimer(void *pvT)
@detail Timer task for disk_timerproc(). It should be called each 10ms. Useful when used along WP and CardPresent
		signals. This task should have the maximum priority in an OS environment.

@return
*/
void vTaskSDMonitor(void *pvM){
	portTickType xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();

	uint8_t sdRealCardStatus;

	while(1){
		if( xSemaphoreTake(sdMonitorSem, 0) ){
			///! Try mounting again
			if(! (appGetSDStatus() & SD_ISMOUNT) ){
				appSDMount(&ssdVolume);
			}
			///! Update logging status
			appSDUpdateLogStatus();
			///! Try opening the file again
			if(appGetSDStatus() == (SD_ISMOUNT | SD_ISLOGGING)){
				appSDOpen(&ssdFile, SD_FILENAME);
			}
			///! Update status
			appSDCardState(&ssdFile, &ssdVolume, SD_FILENAME);
			///! Send last SD Card status
			sdRealCardStatus = appGetSDStatus();
			xQueueSend(sdRealStatus, &sdRealCardStatus, 0);
			///! Release
			xSemaphoreGive(sdMonitorSem);
			///! Next status update
			vTaskDelayUntil(&xLastWakeTime, 5000/portTICK_RATE_MS);
		}
	}

	vTaskDelete(NULL);
}

static void sdStrCpy(uint8_t *sdDestiny, uint8_t *sdSource, uint8_t *sdDestinyOffset){
	uint8_t sdOffset = 0;

	while(*(sdSource + sdOffset) != '\0'){
		*(sdDestiny + (*sdDestinyOffset)) = *(sdSource + sdOffset);
		sdOffset++;
		(*sdDestinyOffset)++;
	}

	*(sdDestiny + (*sdDestinyOffset)) = '\0';
}

static void sdFormatLogSeparator(uint8_t *sdBuffer, uint8_t *sdOffset){
	*(sdBuffer + (*sdOffset)) = '|';
	(*sdOffset)++;
	*(sdBuffer + (*sdOffset)) = ' ';
	(*sdOffset)++;
}

static uint8_t sdFormatLogBuffer(uint8_t *sdBuffer, scDatalogCfg sdNewLogData){
	uint8_t sdOffset = 0;
	uint8_t sdDate[20];

	///! Format PV | MAX | MIN | timestamp
	sdStrCpy(sdBuffer, sdNewLogData.pVarBuffer, &sdOffset);
	sdFormatLogSeparator(sdBuffer, &sdOffset);
	sdStrCpy(sdBuffer, sdNewLogData.pMaxBuffer,  &sdOffset);
	sdFormatLogSeparator(sdBuffer, &sdOffset);
	sdStrCpy(sdBuffer, sdNewLogData.pMinBuffer,  &sdOffset);
	sdFormatLogSeparator(sdBuffer, &sdOffset);
	///! Can be safely used if it's called before writing/creating a file
	tsGetFullDate(sdDate);
	sdStrCpy(sdBuffer, sdDate,  &sdOffset);
	*(sdBuffer + sdOffset) = '\r';
	sdOffset++;
	*(sdBuffer + sdOffset) = '\n';

	return sdOffset;
}

void vTaskSDMLogging(void *pvL){
	portTickType xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();

	scDatalogCfg scNewLogData;
	uint32_t sdLogInterval = 5000; 		///! Default value
	uint8_t sdBuffer[100];
	uint8_t sdBufLen;

	while(1){
		if( xSemaphoreTake(sdMonitorSem, 0) ){
			///! appStack new data
			if(uxQueueMessagesWaiting(sdQueueLogData) > 0){
				xQueueReceive(sdQueueLogData, &scNewLogData, portMAX_DELAY);
				///! Update log interval
				sdLogInterval = scNewLogData.interval;
				///! Write new data to the SD card
				if(appGetSDStatus() == (SD_ISLOGGING | SD_FILE_EXISTS | SD_ISMOUNT)){
					sdBufLen = sdFormatLogBuffer(sdBuffer, scNewLogData);
					///! Do not interrupt while writing the SD card
					portENTER_CRITICAL();
					appSDWrite(&ssdFile, sdBuffer, sdBufLen);
					portEXIT_CRITICAL();
				}
			}
			///! Release
			xSemaphoreGive(sdMonitorSem);
		}
		///! Next status update
		vTaskDelayUntil(&xLastWakeTime, sdLogInterval/portTICK_RATE_MS);
	}

	vTaskDelete(NULL);
}

/**
@fn void appSDHardwareInit(void)
@detail SD Card SPI communication interface configuration.

@warning This function is hardware dependent. Refer to FatFS.

@return
*/
void appSDHardwareInit(void){
    ///! SSP1 setting for SD card
    Board_SSP_Init(LPC_SSP1);
    Chip_SSP_Init(LPC_SSP1);
    Chip_SSP_Enable(LPC_SSP1);
}

/**
@fn uint8_t appGetSDStatus(void)
@detail Returns the current SD Card slot status.

@return sdCardStatus Returns the entire status flag.
*/
uint8_t appGetSDStatus(void){
	return sdCardStatus;
}

/** Private functions
 */

static void appSDUpdateLogStatus(void){
	scDatalogCfg scPeekNewLogData;

	if(uxQueueMessagesWaiting(sdQueueLogData) > 0){
		xQueuePeek(sdQueueLogData, &scPeekNewLogData, 0);

		if(scPeekNewLogData.isLogging){
			sdCardStatus |= SD_ISLOGGING;
		} else {
			sdCardStatus &= ~SD_ISLOGGING;
		}
	}
}
/**
@fn static int appSDMount(void)
@detail FatFS mount function (enclosed in a friendly app call). By default is forced to be mounted the default
		drive.

@warning This function is hardware dependent. Refer to FatFS. Also, the SD card must be previously formatted.
@param *appSD pointer to the filesystem volume to mount.
@return FRESULT (refer to Fatfs) or -1 (if malloc couldn't allocate sufficient memory)
*/
static int appSDMount(FATFS *appLogv){
	FRESULT sdResult;
	sdResult = f_mount(appLogv, "", 1);

	if(sdResult == FR_OK){
		sdCardStatus |= SD_ISMOUNT;
		return FR_OK;
	} else {
		sdCardStatus &= ~SD_ISMOUNT;
		return sdResult;
	}
}

/**
@fn static void appSDUnmount(FATFS **appLogv)
@detail Unmount the SD Card and free the allocated memory.

@param **appLogv pointer to the filesystem structure.
@return
*/
static void appSDUnmount(FATFS *appLogv){
	if(appGetSDStatus() & SD_ISMOUNT){
		f_mount(0, "", 0);}

	sdCardStatus &= ~SD_ISMOUNT;
}

/**
@fn uint8_t appSDOpen(FIL *appFile, const char *appPath)
@detail FatFS open/create file function. By default if the file doesn't exist is created
		else is opened.

@param **appSD pointer to the file structure.
@param *appPath filename string.
@return FRESULT (refer to Fatfs) or -1 (if malloc couldn't allocate sufficient memory)
*/
static int appSDOpen(FIL *appFile, const char *appPath)
{
	FRESULT sdResult;
	sdResult = f_open(appFile, appPath, FA_WRITE| FA_READ | FA_OPEN_APPEND);

	if(sdResult == FR_OK){
		sdCardStatus |= SD_FILE_EXISTS;
		return FR_OK;
	} else {
		sdCardStatus &= ~SD_FILE_EXISTS;
		return sdResult;
	}
}

/**
@fn static void appSDClose(FIL **appFile)
@detail Close the SD Card's file structure and free the allocated memory.

@param **appFile pointer to the file structure.
@return
*/
static void appSDClose(FIL *appFile){
	if(appGetSDStatus() & SD_FILE_EXISTS){
		f_close(appFile);}

	sdCardStatus &= ~SD_FILE_EXISTS;
}

/**
@fn static void appSDCardState(FATFS **appLogv, FIL **appFile, const char *appPath)
@detail SD Card status monitor. This function should be called periodically to verify the
		actual SD Card slot status in order to close and free any taken resources.

@param **appLogv pointer to the filesystem structure.
@param **appFile pointer to the file structure.
@param *appPath logging name file.
@return
*/
static void appSDCardState(FATFS *appLogv, FIL *appFile, const char *appPath){
	FRESULT sdResult;
	char sdLabel[13];
	DWORD sdSerial;

	///! Update mount status
	sdResult = f_getlabel("0:", sdLabel, &sdSerial);
	if(sdResult != FR_OK){
		appSDClose(appFile);
		appSDUnmount(appLogv);
	}
}

/**
@fn static uint8_t appSDWrite(FIL **appFile, const char *appFrom, UINT fromBytes)
@detail FatFS write data function. This function writes and syncs the data immediately.
		This should be appropriate for slow writing intervals.

@param **appFile pointer to the file structure.
@param *appFrom pointer to the data object to be written.
@return VLEFT if there's remaining space in the SD Card and VFULL if not.
*/
static uint8_t appSDWrite(FIL *appFile, const uint8_t *appFrom, UINT fromBytes){
	UINT sdBytes;

	///! Write some data file and immediately flush it in order to secure it
	f_write(appFile, appFrom, (UINT)fromBytes, &sdBytes);
	f_sync(appFile);

	if(sdBytes < fromBytes)
		return SD_VFULL;
	else
		return SD_VLEFT;
}
