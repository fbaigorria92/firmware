/*
 * Pulsadores.c
 *
 *  Created on: 9 de jun. de 2020
 *      Author: nicob
 */

#include "../inc/switches.h"

xSemaphoreHandle		semaphoreSwitchPress;
xQueueHandle			colaSwitchEstable;

void Pulsadores_Init(void){
	//Chip_GPIO_Init(LPC_GPIO);

	//Pulsador
	Chip_IOCON_PinMux(LPC_IOCON,SW1_PORT,SW1_PIN,IOCON_MODE_PULLUP,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,SW2_PORT,SW2_PIN,IOCON_MODE_PULLUP,IOCON_FUNC0);
	//Pulsador como entrada
	Chip_GPIO_SetPinDIRInput(LPC_GPIO,SW1_PORT,SW1_PIN);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO,SW2_PORT,SW2_PIN);

	vSemaphoreCreateBinary(semaphoreSwitchPress);
	xSemaphoreTake(semaphoreSwitchPress,0);

	colaSwitchEstable = xQueueCreate(1,sizeof(uint8_t));

	xTaskCreate(vTaskDebounce, (signed const char *) "vTaskDebounce", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+3, 0);

	//Chip_GPIOINT_Init(LPC_GPIOINT);
	Chip_GPIOINT_SetIntFalling(LPC_GPIOINT,GPIOINT_PORT0,(1<<SW1_PIN) | (1<<SW2_PIN));

	NVIC_ClearPendingIRQ(EINT3_IRQn);
	NVIC_SetPriority(EINT3_IRQn,10);
	NVIC_EnableIRQ(EINT3_IRQn);
}

void EINT3_IRQHandler(void){
	static portBASE_TYPE xSwitchPress;
	uint32_t status = Chip_GPIOINT_GetStatusFalling(LPC_GPIOINT,GPIOINT_PORT0);

	if (status & (1<<SW1_PIN)){
		Chip_GPIOINT_ClearIntStatus(LPC_GPIOINT, GPIOINT_PORT0, (1<<SW1_PIN));
		xSemaphoreGiveFromISR(semaphoreSwitchPress,&xSwitchPress);
		portEND_SWITCHING_ISR(xSwitchPress);
	}

	if (status & (1<<SW2_PIN)){
		Chip_GPIOINT_ClearIntStatus(LPC_GPIOINT, GPIOINT_PORT0, (1<<SW2_PIN));
		xSemaphoreGiveFromISR(semaphoreSwitchPress,&xSwitchPress);
		portEND_SWITCHING_ISR(xSwitchPress);
	}

	NVIC_ClearPendingIRQ(EINT3_IRQn);
}

uint8_t Leer_Pulsadores (void){
	uint8_t pulsador = SWNULL;
	if(uxQueueMessagesWaiting(colaSwitchEstable) > 0){
		xQueueReceive(colaSwitchEstable,&pulsador,portMAX_DELAY);
	}

	return pulsador;
}

void vTaskDebounce(void *pV){
	uint8_t pulsador;
	xSemaphoreTake(semaphoreSwitchPress,0);

	while(1){
		xSemaphoreTake(semaphoreSwitchPress,portMAX_DELAY);
		vTaskDelay(DEBOUNCE_TIME/portTICK_RATE_MS);

		///! KEY 1
		if(Chip_GPIO_GetPinState(LPC_GPIO, SW1_PORT, SW1_PIN) == PRESS){
			pulsador = SW1;
			xQueueSend(colaSwitchEstable,&pulsador,portMAX_DELAY);
		}
		///! KEY2
		if(Chip_GPIO_GetPinState(LPC_GPIO, SW2_PORT, SW2_PIN) == PRESS){
			pulsador = SW2;
			xQueueSend(colaSwitchEstable,&pulsador,portMAX_DELAY);
		}

		//xSemaphoreTake(semaphoreSwitchPress,0);
	}

	vTaskDelete(NULL);
}
