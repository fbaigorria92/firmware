 /**
	\file medicion.c
	\brief Funciones de conversion de la medicion de Sensor GP2Y0A02YKF0F
	\details ECUACION DE GRADO 4 LEVANTANDO CURVA POR GSHEET "LAB 2018.06.01 ecuaciones y curvas GP2Y0A02YKF0F"
	\author UTN_ME1_R4051_Grupo1
	\date 2018.10.19
*/

//#include <math.h>
#include "medicion.h"

static float ecStandard[] = {
		0.0, 280.0, 963.0, 1097.0, 1536.0, 2106.0,
		2215.0, 3720.0, 4883.0, 5354.0, 5714.0
};
static float Counts[]= {
		0.0, 4220.0, 9333.0, 10820.0, 12240.0, 14111.0,
		15760.0, 18550.0, 19960.0, 20930.0, 21130.0
};

//static float ecStandard[] = {
//		5714.0, 5354.0, 4883.0, 3720.0, 2215.0,
//		2106.0, 1536.0, 1097.0, 963.0, 280.0
//};
//static float Counts[]= {
//		21130.0, 20930.0, 19960.0, 18550.0, 15760.0,
//		14111.0, 12240.0, 10820.0, 9333.0, 4220.0
//};

float countsToEC(float counts){
	float ec = 0;
	float m, b;
	int i;

		for (i = 0 ; i < ( sizeof(ecStandard)/sizeof(float) ) - 1; i++){
			if (Counts[i+1] > counts){
				ec = counts - Counts[i];
				ec *= (ecStandard[i+1] - ecStandard[i]);
				ec /= (Counts[i+1] - Counts[i]);
				ec += ecStandard[i];
				break;
			}
		}
	return ec;
}


//	for (i = 0 ; i < ( sizeof(ecStandard)/sizeof(float) ) - 1; i++){
//		if (Counts[i+1] < counts){
//			ec = counts - Counts[i];
//			ec *= (ecStandard[i+1] - ecStandard[i]);
//			ec /= (Counts[i+1] - Counts[i]);
//			ec += ecStandard[i];
//			break;
//		}
//	}


//if(counts < Counts[i+1]){
//	m = (ecStandard[i+1] - ecStandard[i]) / (Counts[i+1] - Counts[i]);
//	b = ecStandard[i] - m * Counts[i+1];
//	ec = m * counts + ecStandard[i];
//	break;
//}
